//
//  ViewController.m
//  DummyTextView
//
//  Created by Brandon Levasseur on 2/16/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textView.attributedText = [self attributedString];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.textView.contentOffset = CGPointZero;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSAttributedString *)attributedString
{
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"Bulbasaur Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ivysaur Lorem ipsum dolor sit amet, consectetur adipiscing elit. Venusaur Lorem ipsum dolor sit amet, consectetur adipiscing elit. Charmander Lorem ipsum dolor sit amet, consectetur adipiscing elit. Charmeleon Lorem ipsum dolor sit amet, consectetur adipiscing elit. Charizard Lorem ipsum dolor sit amet, consectetur adipiscing elit. Squirtle Lorem ipsum dolor sit amet, consectetur adipiscing elit. Wartortle Lorem ipsum dolor sit amet, consectetur adipiscing elit. Blastoise Lorem ipsum dolor sit amet, consectetur adipiscing elit. Caterpie Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metapod Lorem ipsum dolor sit amet, consectetur adipiscing elit. Butterfree Lorem ipsum dolor sit amet, consectetur adipiscing elit. Weedle Lorem ipsum dolor sit amet, consectetur adipiscing elit. Kakuna Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beedrill Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pidgey Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pidgeotto Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pidgeot Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rattata Lorem ipsum dolor sit amet, consectetur adipiscing elit. Raticate Lorem ipsum dolor sit amet, consectetur adipiscing elit. Spearow Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fearow Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ekans Lorem ipsum dolor sit amet, consectetur adipiscing elit. Arbok Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pikachu Lorem ipsum dolor sit amet, consectetur adipiscing elit. Raichu Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sandshrew Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sandslash Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nidoran Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nidorina Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nidoqueen Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nidoran Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nidorino Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nidoking Lorem ipsum dolor sit amet, consectetur adipiscing elit. Clefairy Lorem ipsum dolor sit amet, consectetur adipiscing elit. Clefable Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulpix Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ninetales Lorem ipsum dolor sit amet, consectetur adipiscing elit. Jigglypuff Lorem ipsum dolor sit amet, consectetur adipiscing elit. Wigglytuff Lorem ipsum dolor sit amet, consectetur adipiscing elit. Zubat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Golbat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Oddish Lorem ipsum dolor sit amet, consectetur adipiscing elit. Gloom Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vileplume Lorem ipsum dolor sit amet, consectetur adipiscing elit. Paras Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parasect Lorem ipsum dolor sit amet, consectetur adipiscing elit. Venonat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Venomoth Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diglett Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dugtrio Lorem ipsum dolor sit amet, consectetur adipiscing elit. Meowth Lorem ipsum dolor sit amet, consectetur adipiscing elit. Persian Lorem ipsum dolor sit amet, consectetur adipiscing elit. Psyduck Lorem ipsum dolor sit amet, consectetur adipiscing elit. Golduck Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mankey Lorem ipsum dolor sit amet, consectetur adipiscing elit. Primeape Lorem ipsum dolor sit amet, consectetur adipiscing elit. Growlithe Lorem ipsum dolor sit amet, consectetur adipiscing elit. Arcanine Lorem ipsum dolor sit amet, consectetur adipiscing elit. Poliwag Lorem ipsum dolor sit amet, consectetur adipiscing elit. Poliwhirl Lorem ipsum dolor sit amet, consectetur adipiscing elit. Poliwrath Lorem ipsum dolor sit amet, consectetur adipiscing elit. Abra Lorem ipsum dolor sit amet, consectetur adipiscing elit. Kadabra Lorem ipsum dolor sit amet, consectetur adipiscing elit. Alakazam Lorem ipsum dolor sit amet, consectetur adipiscing elit. Machop Lorem ipsum dolor sit amet, consectetur adipiscing elit. Machoke Lorem ipsum dolor sit amet, consectetur adipiscing elit. Machamp Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bellsprout Lorem ipsum dolor sit amet, consectetur adipiscing elit. Weepinbell Lorem ipsum dolor sit amet, consectetur adipiscing elit. Victreebel Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tentacool Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tentacruel Lorem ipsum dolor sit amet, consectetur adipiscing elit. Geodude Lorem ipsum dolor sit amet, consectetur adipiscing elit. Graveler Lorem ipsum dolor sit amet, consectetur adipiscing elit. Golem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ponyta Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rapidash Lorem ipsum dolor sit amet, consectetur adipiscing elit. Slowpoke Lorem ipsum dolor sit amet, consectetur adipiscing elit. Slowbro Lorem ipsum dolor sit amet, consectetur adipiscing elit. Magnemite Lorem ipsum dolor sit amet, consectetur adipiscing elit. Magneton Lorem ipsum dolor sit amet, consectetur adipiscing elit. Farfetch?d Lorem ipsum dolor sit amet, consectetur adipiscing elit. Doduo Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dodrio Lorem ipsum dolor sit amet, consectetur adipiscing elit. Seel Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dewgong Lorem ipsum dolor sit amet, consectetur adipiscing elit. Grimer Lorem ipsum dolor sit amet, consectetur adipiscing elit. Muk Lorem ipsum dolor sit amet, consectetur adipiscing elit. Shellder Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cloyster Lorem ipsum dolor sit amet, consectetur adipiscing elit. Gastly Lorem ipsum dolor sit amet, consectetur adipiscing elit. Haunter Lorem ipsum dolor sit amet, consectetur adipiscing elit. Gengar Lorem ipsum dolor sit amet, consectetur adipiscing elit. Onix Lorem ipsum dolor sit amet, consectetur adipiscing elit. Drowzee Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hypno Lorem ipsum dolor sit amet, consectetur adipiscing elit. Krabby Lorem ipsum dolor sit amet, consectetur adipiscing elit. Kingler Lorem ipsum dolor sit amet, consectetur adipiscing elit. Voltorb Lorem ipsum dolor sit amet, consectetur adipiscing elit. Electrode Lorem ipsum dolor sit amet, consectetur adipiscing elit. Exeggcute Lorem ipsum dolor sit amet, consectetur adipiscing elit. Exeggutor Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cubone Lorem ipsum dolor sit amet, consectetur adipiscing elit. Marowak Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hitmonlee Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hitmonchan Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lickitung Lorem ipsum dolor sit amet, consectetur adipiscing elit. Koffing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Weezing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rhyhorn Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rhydon" attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:24], NSForegroundColorAttributeName: [UIColor blueColor]}];
    return attributedString;
}

@end
