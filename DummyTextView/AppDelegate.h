//
//  AppDelegate.h
//  DummyTextView
//
//  Created by Brandon Levasseur on 2/16/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

